﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using Random = System.Random;

public class InputProcessor
{
	private readonly Func<float> _elapsedTime;
	private string _inputtedChars = string.Empty;
	private string _moveLeftWord = string.Empty;
	private string _moveRightWord = string.Empty;
	private string _jumpWord = string.Empty;
	private readonly Random _random;

	private IList<string> _availableWords;

	public event Action<string> OnSuccessfulWordTyped;
	public event Action<string> OnSuccessfulLetterTyped;
	public event Action OnIncorrectWordTyped;

	public InputProcessor(ICollection<string> validWords, Func<float> elapsedTime)
	{
		_elapsedTime = elapsedTime;
		_availableWords = validWords.OrderBy(word => word.Length).ToList();

		var regex = new Regex(@"^[a-zA-Z_]*$");
		if (!validWords.Any(word => regex.IsMatch(word)))
		{
			throw new ArgumentException("invalid char in words");
		}

		if (validWords.Count < 4)
		{
			throw new ArgumentException("to few valid words!");
		}

		_random = new Random();
		_moveLeftWord = GetRandomWord();
		_moveRightWord = GetRandomWord();
		_jumpWord = GetRandomWord();
	}

	public string InputtedChars => _inputtedChars;
	public string MoveLeftWord => _moveLeftWord;
	public string MoveRightWord => _moveRightWord;
	public string JumpWord => _jumpWord;
	public bool CanMoveRight { get; set; } = true;
	public bool CanMoveLeft { get; set; } = true;
	public float TimeToReachMaxDifficulty { get; set; } = 60f;

	public InputResult GetResultFromInput(string input)
	{
		foreach (var inputChar in input)
		{
			if (inputChar >= 'a' && inputChar <= 'z' || inputChar >= 'A' && inputChar <= 'Z' || inputChar == ' ')
			{
				_inputtedChars = _inputtedChars + inputChar;

				OnSuccessfulLetterTyped?.Invoke(_inputtedChars);

				if (CanMoveLeft && MoveLeftWord.Equals(_inputtedChars, StringComparison.InvariantCultureIgnoreCase))
				{
					OnSuccessfulWordTyped?.Invoke(_inputtedChars);

					_inputtedChars = string.Empty;
					_moveLeftWord = GetRandomWord();
					return InputResult.MoveLeft;
				}

				if (CanMoveRight && MoveRightWord.Equals(_inputtedChars, StringComparison.InvariantCultureIgnoreCase))
				{
					OnSuccessfulWordTyped?.Invoke(_inputtedChars);

					_inputtedChars = string.Empty;
					_moveRightWord = GetRandomWord();
					return InputResult.MoveRight;
				}

				if (JumpWord.Equals(_inputtedChars, StringComparison.InvariantCultureIgnoreCase))
				{
					OnSuccessfulWordTyped?.Invoke(_inputtedChars);

					_inputtedChars = string.Empty;
					_jumpWord = GetRandomWord();

					return InputResult.Jump;
				}

				var regex = new Regex($@"^{_inputtedChars}[\w\s]", RegexOptions.IgnoreCase);

				if (CanMoveLeft && regex.IsMatch(MoveLeftWord) || CanMoveRight && regex.IsMatch(MoveRightWord) ||
				    regex.IsMatch(JumpWord))
				{
					continue;
				}

				OnIncorrectWordTyped?.Invoke();

				_inputtedChars = string.Empty;
				return InputResult.SpellingError;
			}
		}

		return InputResult.InputIsPartOfAValidWord;
	}

	private string GetRandomWord()
	{
		var words = GetWordsBasedOnDifficultyLevel().ToList();
		var word = words.ElementAt(_random.Next(0, words.Count));
		while (IsAlreadyInUse(word))
		{
			word = words.ElementAt(_random.Next(0, words.Count));
		}

		return word;
	}

	private IEnumerable<string> GetWordsBasedOnDifficultyLevel()
	{
		var fractionOfListToUse = _elapsedTime() / TimeToReachMaxDifficulty;
		var maxItemInList = (int) (_availableWords.Count * fractionOfListToUse);

		return _availableWords.Take(Mathf.Max(maxItemInList, 8));
	}

	private bool IsAlreadyInUse(string word)
	{
		return MoveLeftWord.Equals(word) || MoveRightWord.Equals(word) || JumpWord.Equals(word);
	}
}