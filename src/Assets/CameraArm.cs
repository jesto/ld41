﻿using UnityEngine;

public class CameraArm : MonoBehaviour
{
	[SerializeField]
	public GameObject Target;
	
	void LateUpdate ()
	{
		transform.position = Target.transform.position;
	}
}
