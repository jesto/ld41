﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {

	public void LoadByIndex(int sceneIndex)
	{
		if(Math.Abs(Time.timeScale) <= float.Epsilon)
		{
			Time.timeScale = 1f;
		}

		SceneManager.LoadScene(sceneIndex);
	}
}
