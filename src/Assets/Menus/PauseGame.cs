﻿using UnityEngine;

public class PauseGame : MonoBehaviour {
	private GameObject _mainMenuPanel;
	private GameDirector _gameDirector;
	private bool _canPause = true;
	
	// Use this for initialization
	void Start () {
		_mainMenuPanel = GameObject.Find("MainMenuPanel");
		_mainMenuPanel.SetActive(false);
		_gameDirector = GameObject.Find("GameDirector").GetComponent<GameDirector>();
		_gameDirector.OnGameOver += DisablePauseMenu;
	}

	private void DisablePauseMenu(uint arg1, uint arg2, uint arg3)
	{
		_canPause = false;
	}

	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Delete)) && _canPause)
		{
			_mainMenuPanel.SetActive(true);
			_gameDirector.Pause();
		}
	}

	public void Unpause()
	{
		_mainMenuPanel.SetActive(false);
		_gameDirector.UnPause();
	}
}
