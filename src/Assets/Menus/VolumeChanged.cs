﻿using UnityEngine;
using UnityEngine.UI;

public class VolumeChanged : MonoBehaviour {
	public Slider MusicSlider;
	public Slider SoundEffectSlider;
	private MusicPlayer _musicPlayer;
	private SoundEffectsPlayer _soundEffectsPlayer;

	void Start () {
		MusicSlider.onValueChanged.AddListener(MusicSliderChanged);
		_musicPlayer = FindObjectOfType<MusicPlayer>();
		MusicSlider.value = _musicPlayer.CurrentVolume;

		SoundEffectSlider.onValueChanged.AddListener(SoundEffectsSliderChanged);
		_soundEffectsPlayer = FindObjectOfType<SoundEffectsPlayer>();
		SoundEffectSlider.value = _soundEffectsPlayer.CurrentVolume;
	}

	private void MusicSliderChanged(float value)
	{
		_musicPlayer.ChangeVolume(value);
	}

	private void SoundEffectsSliderChanged(float value)
	{
		_soundEffectsPlayer.ChangeVolume(value);
	}
}
