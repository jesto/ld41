﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnInput : MonoBehaviour {
	public GameObject SelectedObject;
	private EventSystem _eventSystem;
	private bool _buttonSelected;

	// Use this for initialization
	void Start () {
		_eventSystem = EventSystem.current;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxisRaw("Vertical") != 0 && _buttonSelected == false)
		{
			_eventSystem.SetSelectedGameObject(SelectedObject);
			_buttonSelected = true;
		}
	}

	private void OnDisable()
	{
		_buttonSelected = false;
	}
}
