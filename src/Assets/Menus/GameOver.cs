﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {
	private GameDirector _gameDirector;
	private Text _gameOverScoreUIText;
	private Text _gameOverSpellingErrorsUIText;
	private Text _gameOverCorrectWordsUiText;
	private GameObject _gameOverPanel;
	private MusicPlayer _musicPlayer;

	// Use this for initialization
	void Start () {
		_musicPlayer = FindObjectOfType<MusicPlayer>();

		_gameDirector = GameObject.Find("GameDirector").GetComponent<GameDirector>();
		_gameDirector.OnGameOver += GameIsOver;

		_gameOverScoreUIText = GameObject.Find("GameOverScoreText").GetComponent<Text>();
		_gameOverSpellingErrorsUIText = GameObject.Find("GameOverCorrectWordsCountText").GetComponent<Text>();
		_gameOverCorrectWordsUiText = GameObject.Find("GameOverSpellingErrorsText").GetComponent<Text>();

		_gameOverPanel = GameObject.Find("GameOverPanel");
		_gameOverPanel.SetActive(false);
	}

	private void GameIsOver(uint arg1, uint arg2, uint arg3)
	{
		_musicPlayer.PlayMenuTrack();
		_gameOverPanel.SetActive(true);
		_gameOverScoreUIText.text = arg1.ToString();
		_gameOverSpellingErrorsUIText.text = arg2.ToString();
		_gameOverCorrectWordsUiText.text = arg3.ToString();
	}

	// Update is called once per frame
	void Update () {
		
	}
}
