﻿using System;
using System.Collections;
using UnityEngine;

namespace Player
{
	public class MoveAnimation : MonoBehaviour
	{
		[SerializeField] private AnimationCurve _moveTransition;
		[SerializeField] private float _jumpPower = 8;
		private Rigidbody _rigidBody;
		
		private float _targetX;
		private bool _jumpInProgress = false;
		private Movement _movement;
		private Lane _previousLane;

		void Start()
		{
			_rigidBody = GetComponentInChildren<Rigidbody>();
			
			_movement = GetComponent<Movement>();
			_movement.OnMoveLeft += MovementOnMoveLeft;
			_movement.OnMoveRight += MovementOnMoveRight;
			_movement.OnJump += MovementOnJump;
		}

		private void MovementOnJump()
		{
			if(_jumpInProgress)
				return;
			
			_rigidBody.AddForce(0, _jumpPower, 0, ForceMode.Impulse);
		}

		private void MovementOnMoveLeft(Lane currentLane)
		{
			_previousLane = currentLane;
			
			if (_previousLane == Lane.Right)
				_targetX = 0f;
			else if (_previousLane == Lane.Middle)
				_targetX = -1.5f;
			else
				_targetX = -1.5f;

			StartCoroutine(RunTransition());
		}

		private void MovementOnMoveRight(Lane currentLane)
		{
			_previousLane = currentLane;
			
			if (_previousLane == Lane.Left)
				_targetX = 0f;
			else if (_previousLane == Lane.Middle)
				_targetX = 1.5f;
			else
				_targetX = 1.5f;

			StartCoroutine(RunTransition());
		}

		private IEnumerator RunTransition()
		{
			var transitionTime = 0f;
			float curve;
			
			do
			{
				transitionTime += Time.deltaTime;
				curve = _moveTransition.Evaluate(transitionTime);
				var newX = (_targetX - gameObject.transform.position.x) * curve;
				gameObject.transform.position = new Vector3(
					newX + gameObject.transform.position.x, 
					gameObject.transform.position.y, 
					gameObject.transform.position.z);

				yield return null;
			} while (curve < 1.0f);
		}
		
		private void OnCollisionEnter(Collision other)
		{
			if (other.transform.CompareTag("SideDetector"))
			{
				_movement.CurrentLane = _previousLane;
				_movement.UpdatePossibleMovements();

				if (_previousLane == Lane.Left)
					_targetX = -1.5f;
				else if (_previousLane == Lane.Middle)
					_targetX = 0f;
				else
					_targetX = 1.5f;

				StartCoroutine(RunTransition());
			}
		}
	}
}