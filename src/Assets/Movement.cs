﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// TODO: Refactor this class and MoveAnimation, they need clearer separation of concerns
public class Movement : MonoBehaviour
{
	[SerializeField] private float _timeToReachMaxDifficulty = 60f;

	private Text _inputtedCharsUIText;
	private Text _moveLeftWordUIText;
	private Text _moveRightWordUIText;
	private Text _jumpWordUIText;
	private InputProcessor _inputProcessor;

	public float MovementSpeed = 1f;
	public DifficultyLevel DifficultyLevel = DifficultyLevel.Easy;
	public TextAsset ValidWords;
	public bool IsDevelopment = true;
	private GameDirector _gameDirector;
	private bool _isMoving;

	public event Action<Lane> OnMoveLeft;
	public event Action<Lane> OnMoveRight;
	public event Action OnJump;

	public Lane CurrentLane { get; set; } = Lane.Middle;
	
	void Start()
	{
		_inputtedCharsUIText = GameObject.Find("InputtedChars").GetComponent<Text>();
		_moveLeftWordUIText = GameObject.Find("MoveLeftWord").GetComponent<Text>();
		_moveRightWordUIText = GameObject.Find("MoveRightWord").GetComponent<Text>();
		_jumpWordUIText = GameObject.Find("JumpWord").GetComponent<Text>();
		_gameDirector = GameObject.Find("GameDirector").GetComponent<GameDirector>();

		var splitValidWords = new List<string>();
		splitValidWords.AddRange(ValidWords.text.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim()));

		_inputProcessor = new InputProcessor(splitValidWords, () => _gameDirector.SecondsSinceGameStart())
		{
			TimeToReachMaxDifficulty = _timeToReachMaxDifficulty
		};
		_inputProcessor.OnIncorrectWordTyped += _gameDirector.Score.IncorrectWordTyped;
		_inputProcessor.OnSuccessfulLetterTyped += _gameDirector.Score.SuccessfulLetterTyped;
		_inputProcessor.OnSuccessfulWordTyped += _gameDirector.Score.SuccessfulWordTyped;
		OnJump += _gameDirector.Jumped;

		_moveLeftWordUIText.text = _inputProcessor.MoveLeftWord;
		_moveRightWordUIText.text = _inputProcessor.MoveRightWord;
		_jumpWordUIText.text = _inputProcessor.JumpWord;
	}

	void Update()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + Time.deltaTime * MovementSpeed);

		var inputString = Input.inputString;

		if (IsDevelopment && !_gameDirector.IsPaused)
		{
			if (Input.GetKeyDown(KeyCode.LeftArrow))
				inputString = _inputProcessor.MoveLeftWord;

			if (Input.GetKeyDown(KeyCode.RightArrow))
				inputString = _inputProcessor.MoveRightWord;

			if (Input.GetKeyDown(KeyCode.UpArrow))
				inputString = _inputProcessor.JumpWord;
		}

		var inputResult = _inputProcessor.GetResultFromInput(inputString);
		_inputtedCharsUIText.text = _inputProcessor.InputtedChars;
		
		switch (inputResult)
		{
			case InputResult.MoveLeft:
				_moveLeftWordUIText.text = _inputProcessor.MoveLeftWord;
				OnMoveLeft?.Invoke(CurrentLane);
				CurrentLane--;
				UpdatePossibleMovements();
				break;
			
			case InputResult.MoveRight:
				_moveRightWordUIText.text = _inputProcessor.MoveRightWord;
				OnMoveRight?.Invoke(CurrentLane);
				CurrentLane++;
				UpdatePossibleMovements();
				break;
			
			case InputResult.Jump:
				_jumpWordUIText.text = _inputProcessor.JumpWord;
				OnJump?.Invoke();
				break;
		}

	}

	public void UpdatePossibleMovements()
	{
		switch (CurrentLane)
		{
			case Lane.Left:
				_moveLeftWordUIText.gameObject.SetActive(false);
				_inputProcessor.CanMoveLeft = false;
				break;
			case Lane.Right:
				_moveRightWordUIText.gameObject.SetActive(false);
				_inputProcessor.CanMoveRight = false;
				break;
			case Lane.Middle:
				_moveLeftWordUIText.gameObject.SetActive(true);
				_inputProcessor.CanMoveLeft = true;
				_moveRightWordUIText.gameObject.SetActive(true);
				_inputProcessor.CanMoveRight = true;
				break;
		}
		_isMoving = false;
	}

	private void OnCollisionEnter(Collision other)
	{
		if (other.transform.CompareTag("Obstacle"))
		{
			_gameDirector.GameOver();
		}
	}
}