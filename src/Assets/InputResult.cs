﻿public enum InputResult
{
    InputIsPartOfAValidWord = 0,
    SpellingError,
    MoveLeft,
    MoveRight,
    Jump
}