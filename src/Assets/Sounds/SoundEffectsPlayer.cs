﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsPlayer : MonoBehaviour {

	public static SoundEffectsPlayer Instance => _instance;
	public float CurrentVolume => _audioSource.volume;
	public AudioClip GameOver;
	public AudioClip Jump;
	public AudioClip SpelledWrong;

	private AudioSource _audioSource;
	private static SoundEffectsPlayer _instance;

	void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		_audioSource = _instance.GetComponent<AudioSource>();
		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		_audioSource = GetComponent<AudioSource>();
	}

	public void ChangeVolume(float volume)
	{
		_audioSource.volume = volume;
	}

	public void PlayJumpSound()
	{
		if (_audioSource == null)
		{
			_audioSource = _instance.GetComponent<AudioSource>();
		}

		_audioSource.clip = Jump;
		_audioSource.Play();
	}

	public void PlayGameOverSound()
	{
		if (_audioSource == null)
		{
			_audioSource = _instance.GetComponent<AudioSource>();
		}

		_audioSource.clip = GameOver;
		_audioSource.Play();
	}

	public void PlaySpellingErrorSound()
	{
		if (_audioSource == null)
		{
			_audioSource = _instance.GetComponent<AudioSource>();
		}

		_audioSource.clip = SpelledWrong;
		_audioSource.Play();
	}
}
