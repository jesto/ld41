﻿using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {
	public AudioClip MenuTrack;
	public AudioClip[] InGameTracks;
	public static MusicPlayer Instance => _instance;
	public float CurrentVolume => _audioSource.volume;

	private AudioSource _audioSource;
	private static MusicPlayer _instance;
	private uint _inGameTracksIndex = 0;

	void Awake()
	{
		if (_instance != null && _instance != this)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		_audioSource = _instance.GetComponent<AudioSource>();
		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		if (_audioSource.clip == null)
		{
			_audioSource.clip = MenuTrack;
			PlayMenuTrack();
		}
	}

	public void ChangeVolume(float volume)
	{
		_audioSource.volume = volume;
	}

	public void PlayMenuTrack()
	{
		ChangeTrack(MenuTrack);
	}

	public void PlayLatestPlayedInGameTrack()
	{
		ChangeTrack(InGameTracks[_inGameTracksIndex]);
	}

	public void PlayNextInGameTrack()
	{
		if(_inGameTracksIndex + 1 < InGameTracks.Length)
		{
			_inGameTracksIndex++;
			ChangeTrack(InGameTracks[_inGameTracksIndex]);
		}
	}

	public void ResetInGameTracks()
	{
		_inGameTracksIndex = 0;
		ChangeTrack(InGameTracks[_inGameTracksIndex]);
	}

	private void ChangeTrack(AudioClip song)
	{
		if(_audioSource == null)
		{
			_audioSource = _instance.GetComponent<AudioSource>();
		}

		if (_audioSource.clip == null || _audioSource.clip.name != song.name || !_audioSource.isPlaying)
		{
			_audioSource.Stop();
			_audioSource.clip = song;
			_audioSource.Play();
		}
	}
}
