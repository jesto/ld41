﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
	[SerializeField] private GameObject _player;

	private Text _scoreText;
	private Text _multiplierText;
	private bool _isPaused;
	private MusicPlayer _musicPlayer;
	private SoundEffectsPlayer _soundEffectsPlayer;
	private GameObject _ui;
	public Score Score { get; set; } = new Score();
	public event Action<uint, uint, uint> OnGameOver;
	public bool IsPaused => _isPaused;

	public void Start()
	{
		_ui = GameObject.FindGameObjectWithTag("InGameUI");
		_musicPlayer = FindObjectOfType<MusicPlayer>();
		_soundEffectsPlayer = FindObjectOfType<SoundEffectsPlayer>();

		Score.OnScoreChange += OnScoreChange;
		Score.OnSpellingError += OnSpellingError;
		_scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
		_multiplierText = GameObject.Find("MultiplierText").GetComponent<Text>();

		StartGame();
	}

	private void OnScoreChange(uint score, uint multiplier)
	{
		_scoreText.text = $"SCORE: {score}";
		_multiplierText.text = $"x {multiplier}";

		// speed up the music
		if (Score.CorrectWordsCount % 10 == 0)
		{
			_musicPlayer.PlayNextInGameTrack();
		}
	}

	void Update()
	{
	}

	public void Pause()
	{
		if(IsPaused)
		{
			return;
		}

		_ui.SetActive(false);
		_musicPlayer.PlayMenuTrack();
		_isPaused = true;
		Time.timeScale = 0f;
	}

	public void UnPause()
	{
		_ui.SetActive(true);
		_musicPlayer.PlayLatestPlayedInGameTrack();
		_isPaused = false;
		Time.timeScale = 1f;
	}

	public void StartGame()
	{
		_musicPlayer.ResetInGameTracks();
	}

	public void GameOver()
	{
		_soundEffectsPlayer.PlayGameOverSound();
		OnGameOver?.Invoke(Score.CurrentScore, Score.CorrectWordsCount, Score.SpellingErrorsCount);
		_ui.SetActive(false);
		Pause();
	}

	public float SecondsSinceGameStart()
	{
		return Time.timeSinceLevelLoad;
	}

	public void Jumped()
	{
		_soundEffectsPlayer.PlayJumpSound();
	}

	private void OnSpellingError()
	{
		_soundEffectsPlayer.PlaySpellingErrorSound();
	}
}

public class Score
{
	public uint CurrentScore { get; private set; }
	public uint CurrentMultiplier { get; private set; }
	public uint CorrectWordsCount = 0;
	public uint SpellingErrorsCount = 0;
	public event Action<uint, uint> OnScoreChange;
	public event Action OnSpellingError;

	public void IncorrectWordTyped()
	{
		SpellingErrorsCount++;
		CurrentMultiplier = 0;
		OnScoreChange?.Invoke(CurrentScore, CurrentMultiplier);
		OnSpellingError?.Invoke();
	}

	public void SuccessfulLetterTyped(string letter)
	{
	}

	public void SuccessfulWordTyped(string word)
	{
		CorrectWordsCount++;
		CurrentMultiplier++;
		CurrentScore += (uint) word.Length * CurrentMultiplier;
		
		OnScoreChange?.Invoke(CurrentScore, CurrentMultiplier);
	}
}