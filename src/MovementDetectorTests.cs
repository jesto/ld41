﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class MovementDetectorTests
    {
        [Test]
        public void Given_letter_that_is_part_of_a_valid_word_Then_returns_lettersArePartOfAValidWord()
        {
            // arrange
            var movementDetector = new MovementDetector(new List<string> { "test", "some" });

            // act
            var actual = movementDetector.GetResultFromInput("t");

            // assert
            Assert.That(actual, Is.EqualTo(InputResult.InputIsPartOfAValidWord));
        }
    }
}
