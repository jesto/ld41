﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class MovementDetectorTests
    {
        [Test]
        public void Given_to_few_valid_words_Then_throws_argument_exception()
        {
            // arrange
            var expected = new ArgumentException("to few valid words!");

            // act
            var actual = Assert.Throws<ArgumentException>(() => new InputProcessor(new List<string> { "to", "few", "words" }, () => 0f));

            // assert
            Assert.That(actual.Message, Is.EqualTo(expected.Message));
        }

		[TestCase("t*a")]
		[TestCase("snap!")]
		[TestCase("?snap")]
        public void Given_invalid_chars_in_word_Then_throws_argument_exception(string invalidWord)
        {
            // arrange
            var expected = new ArgumentException("invalid char in words");

            // act
            var actual = Assert.Throws<ArgumentException>(() => new InputProcessor(new List<string> { invalidWord }, () => 0f));

            // assert
            Assert.That(actual.Message, Is.EqualTo(expected.Message));
        }

        [Test]
        public void Given_move_left_word_Then_returns_move_left()
        {
            // arrange
            var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input", "two words" }, () => 0f);
            var originalMoveLeftWord = inputProcessor.MoveLeftWord;

            // act
            var actual = inputProcessor.GetResultFromInput(originalMoveLeftWord);

            // assert
            Assert.That(actual, Is.EqualTo(InputResult.MoveLeft));
            Assert.That(inputProcessor.InputtedChars, Is.Empty);
            Assert.That(inputProcessor.MoveLeftWord, Is.Not.EqualTo(originalMoveLeftWord));
        }

        [Test]
        public void Given_move_right_word_Then_returns_move_right()
        {
            // arrange
            var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input" }, () => 0f);
            var originalMoveRightWord = inputProcessor.MoveRightWord;

            // act
            var actual = inputProcessor.GetResultFromInput(originalMoveRightWord);

            // assert
            Assert.That(actual, Is.EqualTo(InputResult.MoveRight));
            Assert.That(inputProcessor.InputtedChars, Is.Empty);
			Assert.That(inputProcessor.MoveLeftWord, Is.Not.EqualTo(originalMoveRightWord));
        }

        [Test]
        public void Given_parts_of_move_left_word_at_a_time_Then_returns_move_left()
        {
            // arrange
            var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input" }, () => 0f);
            var originalMoveLeftWord = inputProcessor.MoveLeftWord;
            var firstCharInMoveLeftWord = originalMoveLeftWord.Substring(0, 1);
            var restOfMoveLeftWord = originalMoveLeftWord.Substring(1);

            // act
            var firstResult = inputProcessor.GetResultFromInput(firstCharInMoveLeftWord);
            var secondResult = inputProcessor.GetResultFromInput(restOfMoveLeftWord);

            // assert
            Assert.That(firstResult, Is.EqualTo(InputResult.InputIsPartOfAValidWord));
            Assert.That(secondResult, Is.EqualTo(InputResult.MoveLeft));
            Assert.That(inputProcessor.InputtedChars, Is.Empty);
            Assert.That(inputProcessor.MoveLeftWord, Is.Not.EqualTo(originalMoveLeftWord));
        }

		[Test]
        public void Given_parts_of_move_right_word_at_a_time_Then_returns_move_left()
        {
            // arrange
            var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input" }, () => 0f);
            var originalMoveRightWord = inputProcessor.MoveRightWord;
            var firstCharInMoveRightWord = originalMoveRightWord.Substring(0, 1);
            var restOfMoveRightWord = originalMoveRightWord.Substring(1);

            // act
            var firstResult = inputProcessor.GetResultFromInput(firstCharInMoveRightWord);
            var secondResult = inputProcessor.GetResultFromInput(restOfMoveRightWord);

            // assert
            Assert.That(firstResult, Is.EqualTo(InputResult.InputIsPartOfAValidWord));
            Assert.That(secondResult, Is.EqualTo(InputResult.MoveRight));
            Assert.That(inputProcessor.InputtedChars, Is.Empty);
            Assert.That(inputProcessor.MoveRightWord, Is.Not.EqualTo(originalMoveRightWord));
        }

        [Test]
        public void Given_input_that_matches_no_current_word_Then_returns_invalid_input()
        {
            // arrange
            var validWords = new List<string> { "test", "with", "some", "input" };
            var inputProcessor = new InputProcessor(validWords, () => 0f);

            var wordThatIsCurrentlyNotUsed = string.Empty;
            foreach (var validWord in validWords)
            {
                if (validWord != inputProcessor.MoveLeftWord && validWord != inputProcessor.MoveRightWord)
                {
                    wordThatIsCurrentlyNotUsed = validWord;
                    break;
                }
            }
            Assert.That(wordThatIsCurrentlyNotUsed, Is.Not.EqualTo(string.Empty));

            // act
            var result = inputProcessor.GetResultFromInput(wordThatIsCurrentlyNotUsed.Substring(0, 1));

            // assert
            Assert.That(result, Is.EqualTo(InputResult.SpellingError));
            Assert.That(inputProcessor.InputtedChars, Is.Empty);
        }

	    [Test]
	    public void Given_move_left_word_When_move_left_is_disabled_Then_returns_spelling_error()
	    {
		    // arrange
		    var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input" }, () => 0f);
			inputProcessor.CanMoveLeft = false;
			var originalMoveLeftWord = inputProcessor.MoveLeftWord;

		    // act
		    var actual = inputProcessor.GetResultFromInput(originalMoveLeftWord);

		    // assert
		    Assert.That(actual, Is.EqualTo(InputResult.SpellingError));
		    Assert.That(inputProcessor.InputtedChars, Is.Empty);
		    Assert.That(inputProcessor.MoveLeftWord, Is.EqualTo(originalMoveLeftWord));
		}

		[Test]
		public void Given_move_right_word_When_move_right_is_disabled_Then_returns_spelling_error()
		{
			// arrange
			var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input" }, () => 0f);
			inputProcessor.CanMoveRight = false;
			var originalMoveRightWord = inputProcessor.MoveRightWord;

			// act
			var actual = inputProcessor.GetResultFromInput(originalMoveRightWord);

			// assert
			Assert.That(actual, Is.EqualTo(InputResult.SpellingError));
			Assert.That(inputProcessor.InputtedChars, Is.Empty);
			Assert.That(inputProcessor.MoveRightWord, Is.EqualTo(originalMoveRightWord));

		}

		[Test]
		public void Given_move_left_word_as_uppercase_Then_returns_move_left()
		{
			// arrange
			var inputProcessor = new InputProcessor(new List<string> { "test", "with", "some", "input", "two words" }, () => 0f);
			var originalMoveLeftWord = inputProcessor.MoveLeftWord;

			// act
			var actual = inputProcessor.GetResultFromInput(originalMoveLeftWord.ToUpper());

			// assert
			Assert.That(actual, Is.EqualTo(InputResult.MoveLeft));
			Assert.That(inputProcessor.InputtedChars, Is.Empty);
			Assert.That(inputProcessor.MoveLeftWord, Is.Not.EqualTo(originalMoveLeftWord));
		}
	}
}
